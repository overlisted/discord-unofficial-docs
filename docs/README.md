# discord-unofficial-docs

Unofficial documentation for Undocumented Discord APIs. Also contains an outline
of Discord's infrastructure.

## Topics

 - [Relationships](/relationships.html)
 - [Connection Properties](/connection_properties.html)
 - [Mobile Indicator](/mobile_indicator.html)
 - [Lazy Guilds](/lazy_guilds.html)
 - [User Data Collection](/science.html)
 - [Per-Client Statuses](/per-client_status.html)
 - [Guild Features](/guild_features.html)
 - [Discord's Infrastructure](/infrastructure.html)
 - [Guild Sync](/guild_sync.html)
 - [Guild Folders](/guild_folders.html)
 - [User Settings](/user_settings.html)
 - [Remote Auth (Desktop)](/desktop_remote_auth.html)
 - [Remote Auth (Mobile)](/mobile_remote_auth.html)
 - [Custom Status](/custom_status.html)

## Licensing?

![CC0 logo](https://i.creativecommons.org/p/zero/1.0/88x31.png)

To the extent possible under law, [Luna Mendes](https://l4.pm) has waived all
copyright and related or neighboring rights to the Unofficial Discord API
Documentation. This work is published from Brazil.
